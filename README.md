# MOSFET driver box
Design files for a 19" rack mounted box to switch the currents through
up to 8 coil pairs. Each coil can be driver with up to 200A.

## Specifications
* Up to 8 coil pairs, rated for 200A each (limit of the connections
  leads) with the used MOSFETs and diodes.
* Water cooled for compact size
* Component count can be reduced to adjust for control needed. Later
  upgrades are also possible
  - Full current direction control in both coils with dual H-bridge (up
    to 4 of 8)
  - Only current control in second coil (switch between gradient and
    offset fields)
  - Enable only
* Matching driver PCBs allow to control the MOSFETs with minimal amount
  of digital inputs
  - drivers have DC/DC converters on board to provide floating grounds
  - inputs are isolated with opto-couplers
  - 5V power input
  - ~13.5V gate drive voltage

## Structure
* [bom](./bom) Bill of material, adjustable for the amount of coils
  needed
* [datasheets](./datasheets) Backups of the datasheets of the most
  relevant components.
* [images](./images) Pictures as guidance for the assembly
* [mechanical](./mechanical) Inventor files for the box. box.iam is the
  main assembly
* [pcb](./pcb) Kicad files for driver PCBs

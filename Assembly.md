# Box Assembly Instructions
## General
The box can contain MOSFETs for up to 8 coil pairs. The design provides
three different configurations. In the simplest configuration called
*Short only* the current either runs through both coils in a fixed
direction or through a short. The next configuration additionally has a
*Single H-Bridge*, allowing to flip the current in one of the two coils.
This allows to switch between offset fields and gradient fields. The last
configuration has a *Dual H-Bridge* allowing to flip the direction of the
current in both coils. This additionally allows it to reverse the field
direction.

Up to 4 of the 8 positions can be dual H-Bridge configurations or simpler
configurations, the other 4 can only be a short or a single H-Bridge due
to space constraints on the cooler plate.

## Cooler plates
The design uses water cooled aluminium plates from Austerliz Electronic.
The part number is [FK 200.22][0] in 220mm length. When buying them order
them with U shaped profiles and connectors soldered in. Part numbers are
FKA L for the U and FKA 2 for quick connectors to 10x1mm tubing. As the
plates are not flattened by the manufacturer it is important to ask the
workshop to flatten both sides before they drill the holes.

The design uses both sides of the plate with a mirrored layout. Because of
this the tapped holes must be usable from both sides (either tapped all
the way through or from both sides). 2 plates are needed for a full box.

## Components
The design uses MOSFETs and Diodes in SOT-227 package (called miniBLOC ore
ISOTOP by some manufacturers). The best way to check the orientation of
components in this package is to use the mounting holes. One is a hole,
the other is a slot. For components used for this setup the current always
flows from the side with the slot to the side with the hole.

For our construction we used the [IXFN360N10T][1] MOSFETs and
[STPS200170T1][2] diodes. These parts allow for operation with up to 200A,
which is the maximum for this package due to limitiatons on the leads. It
should be possible to combine multiple in parallel for higher currents,
but this is not tested and should be checked before using it.

When mounting the packages to the plate and mounting connections to the
packages it is highly recommended to use a torque limiting screw driver.
For mounting to the plate it ensures a good thermal contact (using a very
thin layer of thermal paste, we used 1.5Nm for this. For mounting to the
contacts it is important to tighten properly, but not over-tighten. 1.3Nm
is what most manufacturers recommend for this task. Also always use spring
split lock washers or lock washers for all screws. Otherwise the thermal
stress can loosen the screws, increasing contact resistance creating a
potential fire hazard.

![Screw connections on a MOSFET](images/MOSFET-Closeup.jpg)

## Assembly
The more complex configurations are extensions of the simpler ones. This
means that only more parts are needed, but none of the parts change their
function.

### Plastic parts
Plastic components are used to hold the M6 nuts for the high current
connectors in place, allowing to tighten them without an opposing wrench.
Below you can see a reference picture with all the different parts placed.
Note that this uses older versions of the parts where the nuts were
embedded into the plastic. This is not recommended as the plastic can
deform over time under the pressure of the tightened connections.

Some of the parts need to be removed while assembling as they block the
access to mounting screws. Also the part in the center middle is only
needed when a short or single H-bridge is assembled on the larger side
where a dual H-bridge would fit. Otherwise the place is needed to fit
components.

![Plastic parts only](images/Plastic-Parts.jpg)

On the picture you can also see the two sides of the plate. On the bottom
is the larger side which can fit the dual H-Bridge. On the top is the
smaller side which only fits a single H-Bridge. You can see this by
checking the spacing of the rows. The row in the center fits the spacing
of the rows below it, but has a larger gap until the next row starts on
the top. Note that everything might be mirrored compared to this images
when mounting components on the other side of the cooling plate.

### Dual H-Bridge
This configurations contains all the components which can be fitted. The
smaller configurations leave out parts, but they stay at their location.
If a component is placed here in a certain orientation it will either be
the same component and orientation or left out in smaller configurations.

Below you find images of all MOSFETs and all diodes in their respective
location. On the location of the mounting holes / slots you can check
their orientation. First place all of them, adding a tiny amount of
thermal grease and fixing them with screws and locking washers. We used an
M4x8 screw with a locking washer and a regular washer.

All MOSFETS:
![All placed MOSFETs](images/Dual-H-Bridge-MOSFETs.jpg)

All Diodes:
![All placed diodes](images/Dual-H-Bridge-Diodes.jpg)

Next connect all the control wires and the copper connection plates for
the lower layer. When using the plates one can not use the screws
delivered together with the MOSFET/diode. Instead use M4x10 (x12 is
already to long), adding a spring split lock washer and a regular washer.
Tighten them to 1.3Nm. When placing the L shaped pieces on the side make
sure to use the correct one for either side. They differ by the connection
on the short side of the L where they connect to the source of the MOSFET.

The cables used are 0.5mm² wires, the black ones are connected to the
respective ground levels, the red ones to the MOSFET gates. Smaller
diameters are acceptable as long as they fit into the used M4 crimping
shoes. Bigger diameters will not fit into the screw terminals on the PCB.
The PCBs are mounted in the front of the box, the connector are
approximately on the position of the U shaped water connections. Cut the
cable to this length with some extra to cut later.

![Everything in the lower level placed](images/Dual-H-Bridge-Lower-Layer.jpg)

Lastly place the components on the second layer. These use the higher
standoffs. Also place the resistors used to dissipate the shutdown
currents. They are not cooled as they only have to accept very short
pulses with low duty cycle.

Below is the final fully populated dual H-Bridge configuration with the
corresponding coil connections labeled.  The big cable shoes are used to
indicate the direction where the wires should go. They should be for M6
holes, we used 25mm² wire for the internal high current connections (cable
shoes used here are Klauke 4R6) . On some locations 45 degrees angled
shoes should be used (Klauke 44R645 for 25mm²).

The assignment of the + and - locations to the coils can be flipped as the
setup is symmetric. The choice is made when connecting the control wires
to the control PCB. It is however recommended to stick to this assignment
to have some consistency between the different assemblies.

![Fully populated dual H-Bridge configuration](images/Dual-H-Bridge.jpg)

### Single H-Bridge
The single H-Bridge configuration skips the bottom row where the coil 1+
connection is located and also drops all the diodes and MOSFETs below the
Power In connection. Coil 1+ is permanently connected to power input on
the adapter board on the back of the box.

The setup on the image below is built on the other side of the plate
imaged above for the dual H-bridge. Because of this the full assembly is
rotated by a 180 degrees.

![Fully assembled single H-Bridge configuration](images/Single-H-Bridge.jpg)

### Short
The short only consists of two MOSFETs and a single diode + resistor. Only
a single ground connection is needed as it is shared between both MOSFETs.
On the adapter board in the back of the box coil 1+ is connected to power
in and coil 1- to coil 2+ (those can also be connected closer towards the
coils).

![Fully assembled short](images/Short.jpg)

# PCB wiring
The PCBs are mounted in the front panel, fixed by the BNC connectors.
When they are placed in the box the screw terminals are accessible from
the top. Connect the wires as indicated on the silk screen on the PCBs.
We used cable ties to keep the groups together and minimise the enclosed
area. Not sure if it is necessary, but it certainly doesn't hurt. On the
bottom side the PCBs are mounted the other way around, such that the
screw terminals are always accessible. This flips the order of the BNC
plugs on the front.

Below you see a picture of the fully assembled top half of the box (2
double H-bridges, 1 single H-bridge and 1 short).  We populated all the
SMT components on the PCBs, but left out the DC-DC converters and the
connectors (screw terminals and BNC). This makes it relatively easy to
upgrade later on.

![Assembled and wired top half](images/Top-Completed.jpg)

![Front of the assembled box](images/Front-Completed.jpg)


 [0]: http://austerlitz-electronic.de/kuehlkoerper/fluessigkeitskuehlung/kuehlprofile/182/fk-200.22
 [1]: https://www.littelfuse.com/products/power-semiconductors/discrete-mosfets/n-channel-trench-gate/gen1/ixfn360n10t.aspx
 [2]: https://www.st.com/en/diodes-and-rectifiers/stps200170tv1.html

Used cable lengths to connect the back with the wire connections.

Back connections are numbered from 1 to 8, starting on the side without water connection. The connections are always as short as possible, i.e. the two lowest numbers connect to the first circuits from the same side top and bottom.

The connections used are as follows (they might differ based on the orientation chosen when mounting the plates.
* Back 1 to first plate Dual-H-Bridge (on top in our case)
* Back 2 to first plate Dual-H-Bridge (on bottom)
* Back 3 to first plate Single-H-Bridge (on top)
* Back 4 to first plate Single-H-Bridge (on bottom)
* Back 5 to second plate Dual-H-Bridge (on top)
* Back 6 to second plate Dual-H-Bridge (on bottom)
* Back 7 to second plate Single-H-Bridge (on top)
* Back 8 to second plate Single-H-Bridge (on bottom)

Wire connections on the back have the following usage labeled from top to bottom (in our orientation)
* 1 on top (in our case) to Power In
* 2 to Coil 1+
* 3 to Coil 1-
* 4 to Coil 2+
* 5 to Coil 2-
* 6 to Power Out

| Plate | In  | 1+  | 1-  | 2+  | 2-  | Out |
|-------|-----|-----|-----|-----|-----|-----|
| 1     | 22  | 22  | 26  | 15  | 35' | 30  |
| 2     | 25  | 22  | 24  | 35' | 15  | 23  |
| 3     | 24  |     | 22  | 36' | 17  | 25  |
| 4     |     |     |     |     |     |     |
| 5     | 20  | 24  | 27  | 16  | 36' | 29  |
| 6     | 26  |     | 23  | 35' | 14  | 22  |
| 7     | 24  |     |     |     | 19  | 26  |
| 8     | 28  |     |     |     | 34' | 27  |

All lengths are given in cm. When a number is followed by a tick it needs a 45 degree cable shoe on the plate end. The measured length is the length to cut the wire to.

Remove about 2cm of insulation on either end for crimping. This is best done using a cutter, pressing from the top and rolling the wire below the cutter. Then pull the first bit with your fingers and use pliers to fully remove it.
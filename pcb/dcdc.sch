EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 18
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR019
U 1 1 6045569E
P 2950 3000
AR Path="/60451830/6045569E" Ref="#PWR019"  Part="1" 
AR Path="/60462D8C/6045569E" Ref="#PWR026"  Part="1" 
AR Path="/60462F0E/6045569E" Ref="#PWR028"  Part="1" 
AR Path="/604630E7/6045569E" Ref="#PWR030"  Part="1" 
AR Path="/604631EE/6045569E" Ref="#PWR032"  Part="1" 
F 0 "#PWR019" H 2950 2850 50  0001 C CNN
F 1 "+5V" H 2965 3173 50  0000 C CNN
F 2 "" H 2950 3000 50  0001 C CNN
F 3 "" H 2950 3000 50  0001 C CNN
	1    2950 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 604556A4
P 2950 3600
AR Path="/60451830/604556A4" Ref="#PWR020"  Part="1" 
AR Path="/60462D8C/604556A4" Ref="#PWR027"  Part="1" 
AR Path="/60462F0E/604556A4" Ref="#PWR029"  Part="1" 
AR Path="/604630E7/604556A4" Ref="#PWR031"  Part="1" 
AR Path="/604631EE/604556A4" Ref="#PWR033"  Part="1" 
F 0 "#PWR020" H 2950 3350 50  0001 C CNN
F 1 "GND" H 2955 3427 50  0000 C CNN
F 2 "" H 2950 3600 50  0001 C CNN
F 3 "" H 2950 3600 50  0001 C CNN
	1    2950 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 604556AB
P 3400 3100
AR Path="/60451830/604556AB" Ref="L1"  Part="1" 
AR Path="/60462D8C/604556AB" Ref="L3"  Part="1" 
AR Path="/60462F0E/604556AB" Ref="L5"  Part="1" 
AR Path="/604630E7/604556AB" Ref="L7"  Part="1" 
AR Path="/604631EE/604556AB" Ref="L9"  Part="1" 
F 0 "L1" V 3590 3100 50  0000 C CNN
F 1 "10u" V 3499 3100 50  0000 C CNN
F 2 "Inductor_SMD:L_1008_2520Metric_Pad1.43x2.20mm_HandSolder" H 3400 3100 50  0001 C CNN
F 3 "~" H 3400 3100 50  0001 C CNN
	1    3400 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 604556B1
P 2950 3300
AR Path="/60451830/604556B1" Ref="C5"  Part="1" 
AR Path="/60462D8C/604556B1" Ref="C16"  Part="1" 
AR Path="/60462F0E/604556B1" Ref="C23"  Part="1" 
AR Path="/604630E7/604556B1" Ref="C30"  Part="1" 
AR Path="/604631EE/604556B1" Ref="C37"  Part="1" 
F 0 "C5" H 3065 3346 50  0000 L CNN
F 1 "10u 50V" H 3065 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 2988 3150 50  0001 C CNN
F 3 "~" H 2950 3300 50  0001 C CNN
	1    2950 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 604556B7
P 3800 3300
AR Path="/60451830/604556B7" Ref="C6"  Part="1" 
AR Path="/60462D8C/604556B7" Ref="C17"  Part="1" 
AR Path="/60462F0E/604556B7" Ref="C24"  Part="1" 
AR Path="/604630E7/604556B7" Ref="C31"  Part="1" 
AR Path="/604631EE/604556B7" Ref="C38"  Part="1" 
F 0 "C6" H 3915 3346 50  0000 L CNN
F 1 "4.7u 50V" H 3915 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 3838 3150 50  0001 C CNN
F 3 "~" H 3800 3300 50  0001 C CNN
	1    3800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3100 3800 3100
Wire Wire Line
	3800 3100 3800 3150
Connection ~ 3800 3100
Wire Wire Line
	3800 3500 3800 3450
Wire Wire Line
	3800 3500 2950 3500
Wire Wire Line
	2950 3500 2950 3450
Connection ~ 3800 3500
Wire Wire Line
	2950 3500 2950 3600
Connection ~ 2950 3500
Wire Wire Line
	2950 3150 2950 3100
Wire Wire Line
	2950 3100 3250 3100
Wire Wire Line
	2950 3000 2950 3100
Connection ~ 2950 3100
$Comp
L Device:C C7
U 1 1 604556CC
P 5400 3300
AR Path="/60451830/604556CC" Ref="C7"  Part="1" 
AR Path="/60462D8C/604556CC" Ref="C18"  Part="1" 
AR Path="/60462F0E/604556CC" Ref="C25"  Part="1" 
AR Path="/604630E7/604556CC" Ref="C32"  Part="1" 
AR Path="/604631EE/604556CC" Ref="C39"  Part="1" 
F 0 "C7" H 5515 3346 50  0000 L CNN
F 1 "1u" H 5515 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5438 3150 50  0001 C CNN
F 3 "~" H 5400 3300 50  0001 C CNN
	1    5400 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 604556DB
P 7100 3300
AR Path="/60451830/604556DB" Ref="C9"  Part="1" 
AR Path="/60462D8C/604556DB" Ref="C20"  Part="1" 
AR Path="/60462F0E/604556DB" Ref="C27"  Part="1" 
AR Path="/604630E7/604556DB" Ref="C34"  Part="1" 
AR Path="/604631EE/604556DB" Ref="C41"  Part="1" 
F 0 "C9" H 7215 3346 50  0000 L CNN
F 1 "100n" H 7215 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7138 3150 50  0001 C CNN
F 3 "~" H 7100 3300 50  0001 C CNN
	1    7100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:L L2
U 1 1 604556E1
P 6150 3100
AR Path="/60451830/604556E1" Ref="L2"  Part="1" 
AR Path="/60462D8C/604556E1" Ref="L4"  Part="1" 
AR Path="/60462F0E/604556E1" Ref="L6"  Part="1" 
AR Path="/604630E7/604556E1" Ref="L8"  Part="1" 
AR Path="/604631EE/604556E1" Ref="L10"  Part="1" 
F 0 "L2" V 6340 3100 50  0000 C CNN
F 1 "10u" V 6249 3100 50  0000 C CNN
F 2 "Inductor_SMD:L_1008_2520Metric_Pad1.43x2.20mm_HandSolder" H 6150 3100 50  0001 C CNN
F 3 "~" H 6150 3100 50  0001 C CNN
	1    6150 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 3100 6400 3100
Wire Wire Line
	7100 3100 7100 3150
Wire Wire Line
	6000 3100 5750 3100
Wire Wire Line
	7100 3450 7100 3500
Wire Wire Line
	5400 3150 5400 3100
Connection ~ 5400 3100
Wire Wire Line
	5400 3450 5400 3500
Connection ~ 5400 3500
Wire Wire Line
	5400 3500 5750 3500
Wire Wire Line
	5750 3150 5750 3100
Connection ~ 5750 3100
Wire Wire Line
	5750 3100 5400 3100
Wire Wire Line
	5750 3450 5750 3500
Connection ~ 5750 3500
Text HLabel 9650 3900 2    50   Output ~ 0
GNDISO
Text Notes 6050 3650 0    50   ~ 0
LP ~~15kHz
Text HLabel 9650 3100 2    50   Output ~ 0
VISO
$Comp
L mosfet-driver-rescue:C_Polarized-Device C10
U 1 1 608E2F59
P 9050 3500
AR Path="/60451830/608E2F59" Ref="C10"  Part="1" 
AR Path="/60462D8C/608E2F59" Ref="C21"  Part="1" 
AR Path="/60462F0E/608E2F59" Ref="C28"  Part="1" 
AR Path="/604630E7/608E2F59" Ref="C35"  Part="1" 
AR Path="/604631EE/608E2F59" Ref="C42"  Part="1" 
AR Path="/608E2F59" Ref="C?"  Part="1" 
F 0 "C10" H 9168 3546 50  0000 L CNN
F 1 "100u" H 9168 3455 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.8" H 9088 3350 50  0001 C CNN
F 3 "~" H 9050 3500 50  0001 C CNN
	1    9050 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 60953C54
P 5750 3300
AR Path="/60451830/60953C54" Ref="D2"  Part="1" 
AR Path="/60462D8C/60953C54" Ref="D3"  Part="1" 
AR Path="/60462F0E/60953C54" Ref="D4"  Part="1" 
AR Path="/604630E7/60953C54" Ref="D5"  Part="1" 
AR Path="/604631EE/60953C54" Ref="D6"  Part="1" 
F 0 "D2" V 5704 3380 50  0000 L CNN
F 1 "MMSZ16" V 5795 3380 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 5750 3300 50  0001 C CNN
F 3 "~" H 5750 3300 50  0001 C CNN
	1    5750 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 3500 6400 3500
$Comp
L mosfet-driver-rescue:RK-0515S-Converter_DCDC U5
U 1 1 606E14CF
P 4800 3300
AR Path="/60451830/606E14CF" Ref="U5"  Part="1" 
AR Path="/60462D8C/606E14CF" Ref="U8"  Part="1" 
AR Path="/60462F0E/606E14CF" Ref="U10"  Part="1" 
AR Path="/604630E7/606E14CF" Ref="U12"  Part="1" 
AR Path="/604631EE/606E14CF" Ref="U14"  Part="1" 
AR Path="/606E14CF" Ref="U?"  Part="1" 
F 0 "U5" H 4800 3767 50  0000 C CNN
F 1 "RK-0515S" H 4800 3676 50  0000 C CNN
F 2 "footprints:Converter_DCDC_RECOM_RK-xxxxS_Single_THT" H 4800 3300 50  0001 C CNN
F 3 "https://recom-power.com/pdf/Econoline/RK_RH.pdf" H 4800 3300 50  0001 C CNN
	1    4800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3100 5250 3100
Wire Wire Line
	5200 3500 5400 3500
Wire Wire Line
	3800 3100 4300 3100
Wire Wire Line
	3800 3500 4400 3500
$Comp
L Device:C C4
U 1 1 606E2A7E
P 4800 2550
AR Path="/60451830/606E2A7E" Ref="C4"  Part="1" 
AR Path="/60462D8C/606E2A7E" Ref="C15"  Part="1" 
AR Path="/60462F0E/606E2A7E" Ref="C22"  Part="1" 
AR Path="/604630E7/606E2A7E" Ref="C29"  Part="1" 
AR Path="/604631EE/606E2A7E" Ref="C36"  Part="1" 
F 0 "C4" V 4548 2550 50  0000 C CNN
F 1 "2n2" V 4639 2550 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L13.0mm_W4.0mm_P10.00mm_FKS3_FKP3_MKS4" H 4838 2400 50  0001 C CNN
F 3 "~" H 4800 2550 50  0001 C CNN
	1    4800 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 2550 4300 2550
Wire Wire Line
	4300 2550 4300 3100
Connection ~ 4300 3100
Wire Wire Line
	4300 3100 4400 3100
Wire Wire Line
	4950 2550 5250 2550
Wire Wire Line
	5250 2550 5250 3100
Connection ~ 5250 3100
Wire Wire Line
	5250 3100 5400 3100
Text Notes 3000 2150 0    50   ~ 0
EMI filter based on datasheet values,\nslightly tweaked to whats available (10u instead of 12u)
$Comp
L Connector:TestPoint_2Pole TP2
U 1 1 60784298
P 9500 3500
AR Path="/60462D8C/60784298" Ref="TP2"  Part="1" 
AR Path="/60451830/60784298" Ref="TP1"  Part="1" 
AR Path="/60462F0E/60784298" Ref="TP3"  Part="1" 
AR Path="/604630E7/60784298" Ref="TP4"  Part="1" 
AR Path="/604631EE/60784298" Ref="TP5"  Part="1" 
F 0 "TP1" V 9546 3558 50  0000 L CNN
F 1 "TestPoint" V 9455 3558 50  0000 L CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 9500 3500 50  0001 C CNN
F 3 "~" H 9500 3500 50  0001 C CNN
	1    9500 3500
	0    1    -1   0   
$EndComp
Text Notes 5600 2750 0    50   ~ 0
LQH2HPN100MJRL (10uH)
$Comp
L mosfet-driver-rescue:LM2931-ADJ_SO8-Regulator_Linear U4
U 1 1 609D1842
P 8000 3200
AR Path="/60451830/609D1842" Ref="U4"  Part="1" 
AR Path="/60462D8C/609D1842" Ref="U7"  Part="1" 
AR Path="/60462F0E/609D1842" Ref="U9"  Part="1" 
AR Path="/604630E7/609D1842" Ref="U11"  Part="1" 
AR Path="/604631EE/609D1842" Ref="U13"  Part="1" 
AR Path="/609D1842" Ref="U4"  Part="1" 
F 0 "U4" H 8000 3542 50  0000 C CNN
F 1 "LM2931ACDR2G" H 8000 3451 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8000 3425 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm2931-n.pdf" H 8000 3200 50  0001 C CNN
	1    8000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 3100 7100 3100
Connection ~ 7100 3100
Wire Wire Line
	7600 3200 7500 3200
Wire Wire Line
	7500 3200 7500 3500
Wire Wire Line
	7500 3500 7100 3500
Connection ~ 7100 3500
$Comp
L Device:R R2
U 1 1 609D7E10
P 8650 3300
AR Path="/60451830/609D7E10" Ref="R2"  Part="1" 
AR Path="/60462D8C/609D7E10" Ref="R6"  Part="1" 
AR Path="/60462F0E/609D7E10" Ref="R8"  Part="1" 
AR Path="/604630E7/609D7E10" Ref="R10"  Part="1" 
AR Path="/604631EE/609D7E10" Ref="R12"  Part="1" 
F 0 "R2" H 8720 3346 50  0000 L CNN
F 1 "1k" H 8720 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8580 3300 50  0001 C CNN
F 3 "~" H 8650 3300 50  0001 C CNN
	1    8650 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 609D895A
P 8650 3700
AR Path="/60451830/609D895A" Ref="R3"  Part="1" 
AR Path="/60462D8C/609D895A" Ref="R7"  Part="1" 
AR Path="/60462F0E/609D895A" Ref="R9"  Part="1" 
AR Path="/604630E7/609D895A" Ref="R11"  Part="1" 
AR Path="/604631EE/609D895A" Ref="R13"  Part="1" 
F 0 "R3" H 8720 3746 50  0000 L CNN
F 1 "10k" H 8720 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8580 3700 50  0001 C CNN
F 3 "~" H 8650 3700 50  0001 C CNN
	1    8650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3100 8650 3100
Wire Wire Line
	8650 3100 8650 3150
Wire Wire Line
	8400 3300 8500 3300
Wire Wire Line
	8500 3300 8500 3500
Wire Wire Line
	8500 3500 8650 3500
Wire Wire Line
	8650 3500 8650 3450
Wire Wire Line
	8650 3500 8650 3550
Connection ~ 8650 3500
Text Notes 8450 3000 0    50   ~ 0
Reference Voltage: 1.14V-1.26V\nVout = 12.54-13.86V
Wire Wire Line
	7500 3500 7500 3600
Wire Wire Line
	7500 3600 8000 3600
Wire Wire Line
	8000 3600 8000 3500
Connection ~ 7500 3500
Wire Wire Line
	8000 3600 8000 3900
Wire Wire Line
	8000 3900 8650 3900
Wire Wire Line
	8650 3900 8650 3850
Connection ~ 8000 3600
Wire Wire Line
	8650 3100 9050 3100
Connection ~ 8650 3100
Wire Wire Line
	9050 3650 9050 3900
Wire Wire Line
	9050 3900 8650 3900
Connection ~ 8650 3900
Wire Wire Line
	9650 3100 9500 3100
Connection ~ 9050 3100
Wire Wire Line
	9050 3100 9050 3350
Wire Wire Line
	9500 3300 9500 3100
Connection ~ 9500 3100
Wire Wire Line
	9500 3100 9050 3100
Wire Wire Line
	9500 3700 9500 3900
Wire Wire Line
	9500 3900 9050 3900
Connection ~ 9050 3900
Wire Wire Line
	9650 3900 9500 3900
Connection ~ 9500 3900
Text Notes 8050 4700 0    50   ~ 0
output cap determines stability of the\nregulator. Exact values are manufacturer specific\n\nUsing ON Semi LM2931ACDR2G ESR < 0.5Ohm.\nSmaller reduces noise.\nSome (e.g. TI) also have a minimal ESR\n\nE.g. Panasonic EEEFT1V101AP fits (~~300mOhm)
$Comp
L Device:C C8
U 1 1 60A02FE7
P 6400 3300
AR Path="/60451830/60A02FE7" Ref="C8"  Part="1" 
AR Path="/60462D8C/60A02FE7" Ref="C19"  Part="1" 
AR Path="/60462F0E/60A02FE7" Ref="C26"  Part="1" 
AR Path="/604630E7/60A02FE7" Ref="C33"  Part="1" 
AR Path="/604631EE/60A02FE7" Ref="C40"  Part="1" 
F 0 "C8" H 6515 3346 50  0000 L CNN
F 1 "10u" H 6515 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 6438 3150 50  0001 C CNN
F 3 "~" H 6400 3300 50  0001 C CNN
	1    6400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3150 6400 3100
Connection ~ 6400 3100
Wire Wire Line
	6400 3100 7100 3100
Wire Wire Line
	6400 3450 6400 3500
Connection ~ 6400 3500
Wire Wire Line
	6400 3500 7100 3500
Text Notes 7400 2650 0    50   ~ 0
LDO with high PSRR (~~60dB at 100kHz)\nto reduce switching mode spikes
$EndSCHEMATC

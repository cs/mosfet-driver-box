# MOSFET driver PCB
This PCB is designed to drive N-channel MOSFETs to control the current
through a coil pair. With all parts assembled it can switch the direction
of the field, switch between Helmholtz and Anti-Helmholtz configuration or
short the coil.

## Inputs
All MOSFETs are controlled by only three BNC inputs. Every input is
individually isolated using an opto-coupler.
* Enable: Only when this is high any current flows through the coil. If it
  is low the short is activated and all other  output MOSFETs (those with
  drain connected to the output GND) are disabled.
* Helmholtz / Gradient switch: Switches the current flow
  through the second coil, changing the field from gradient to offset. Has
  no effect when enable is low
* Flip: Inverts the polarity of the magnetic field. This flips the
  direction of the current in both coils. Has no effect when enable is
  low. When the gradient switch is also enabled the second coil is
  effectively switched twice, i.e. only the first coil is flipped.
  
## Outputs
The board has five output groups which are isolated from each other. This
is necessary as the MOSFETs must be driver relative to their own GND,
located at the drain of the MOSFET. The outputs of each group share this
GND point. They are named by the connection on this point, e.g. `GND 2-`
is connected to the (arbitrarily chosen) minus pole of the second coil.

The control outputs should be directly connected to the gate ports of the
MOSFETs. They are labeled by their input (source) and output (drain)
connections, e.g. `2+ Out` connects the plus pole of the second coil with
the output to the negative terminal of the power supply.

## Logic table
Logic table of the MOSFET states. 0 is logic input low or MOSFET off,
1 is logic input high or MOSFET enabled.
| Enable | Offs/Grad | Flip | Short | 2- Out | 2+ Out | 1- 2+ | 1+ 2+ | 1- 2- | 1+ 2- | IN 1+ | IN 1- |
|--------|-----------|------|-------|--------|--------|-------|-------|-------|-------|-------|-------|
| 0      | 0         | 0    | 1     | 0      | 0      | 1     | 0     | 0     | 0     | 1     | 0     |
| 0      | 1         | 0    | 1     | 0      | 0      | 0     | 0     | 1     | 0     | 1     | 0     |
| 0      | 0         | 1    | 1     | 0      | 0      | 0     | 0     | 0     | 1     | 0     | 1     |
| 0      | 1         | 1    | 1     | 0      | 0      | 0     | 1     | 0     | 0     | 0     | 1     |
| 1      | 0         | 0    | 0     | 1      | 0      | 1     | 0     | 0     | 0     | 1     | 0     |
| 1      | 1         | 0    | 0     | 0      | 1      | 0     | 0     | 1     | 0     | 1     | 0     |
| 1      | 0         | 1    | 0     | 0      | 1      | 0     | 0     | 0     | 1     | 0     | 1     |
| 1      | 1         | 1    | 0     | 1      | 0      | 0     | 1     | 0     | 0     | 0     | 1     |

### In coils
| Enable | Gradient | Flip | Short | Coil 1   | Coil 2   |
|--------|----------|------|-------|----------|----------|
| 0      | X        | X    | On    | Off      | Off      |
| 1      | 0        | 0    | Off   | Forward  | Forward  |
| 1      | 1        | 0    | Off   | Forward  | Reversed |
| 1      | 0        | 1    | Off   | Reversed | Reversed |
| 1      | 1        | 1    | Off   | Reversed | Forward  |

## Notes
* When building a driver for a reduced assembly with less MOSFETs leave
  out the corresponding DC/DC converters to disable the drivers. When
  building a single H-bridge only the first three are needed, for the
  simple enable only teh first converter is needed. We also left out the
  corresponding connectors to make it clear that parts are not
  populated. We did however place all the SMT components beforehand to
  make it easier to upgrade later on.
* The boards needs a relatively high current to start up. When placing
  all 5 converters about 800mA are needed. Afterwards the current should
  drop to about 250mA. With lower current the board will not start and
  will continue to draw the high current.  This is mainly an issue when
  supplying the board from a lab supply with current limit.
  In our box we added a 15W (5V 3A) power supply which is enough to
  supply the current for 7 boards with 25 converts total.

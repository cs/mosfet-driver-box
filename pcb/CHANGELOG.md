# Design errors
## Revision 3 and 4
* Both versions have a bad connection from the output of the flip
  opto-coupler to the control input of the IN 1- MOSFET driver (U22 in
  revision 4). To fix this add a wire between pad 1 of R38 (in rev4) and
  pad 2 of U22 in rev4).

# Changelog
## Changes from Revision 3 to Revision 4
* Added tiny cutouts to the back edges. On the center positions in the box can
  collide a bit with the aluminium mounting plates for the coolers.
* Moved BNC connectors to front by a bit to have them flush with the front
  panel for our standard connector (TE 1-1337543-0)
* Added cuts in the ground planes to separate the GND between the DC/DC
  converter and the linear regulator from the GND for everything behind
  the regulator. This forces all ground currents to pass by the regulator.
  Moved some electrolytic capacitors on the back side, on the front side
  no parts were moved (stancil can be used for revision 3 and revision 4)

## Changes from Revision 2 to Revision 3
* Added LM2931 linear regulators behind the DC/DC converters to suppress
  switching mode noise. Due to this the previously optional electrolytic
  capacitor is now mandatory and SMT to make it fit. There are more parts
  on the back side of the PCB now.
* Reduced width by 0.5mm on either side to make increase clearance when
  mounting to 21 TE front panel. Connector positions are not changed.
* Placed the power LED such that it could also be guided to the front
  panel using a light guide.

## Changes from Revision 1 to Revision 2
* +Vout and -Vout of the DCDC converters had their pin swapped
* Replaced 2N7002 (N-MOS) with BC817 (NPN) for diode light. Also added
  resistor before the gate.
* Changed 220pF caps to 330pF (capacity already in use)

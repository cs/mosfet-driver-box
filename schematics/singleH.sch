EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2500 2500 0    50   Input ~ 0
PowerIn
Text HLabel 2500 5500 0    50   Output ~ 0
PowerOut
Text HLabel 8000 2500 2    50   UnSpc ~ 0
Coil1+
Text HLabel 8000 3000 2    50   UnSpc ~ 0
Coil1-
Text HLabel 8000 4500 2    50   UnSpc ~ 0
Coil2-
Text HLabel 8000 4000 2    50   UnSpc ~ 0
Coil2+
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5FBC1CD1
P 5900 5200
F 0 "Q?" H 6104 5246 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 6104 5155 50  0000 L CNN
F 2 "" H 6100 5300 50  0001 C CNN
F 3 "~" H 5900 5200 50  0001 C CNN
	1    5900 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 5500 3000 5500
Wire Wire Line
	4500 5500 4500 5400
Wire Wire Line
	4500 5500 6000 5500
Wire Wire Line
	6000 5500 6000 5400
Connection ~ 4500 5500
Wire Wire Line
	6000 4500 7500 4500
Wire Wire Line
	8000 4000 7000 4000
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5FBC56B8
P 2900 5200
F 0 "Q?" H 3104 5246 50  0000 L CNN
F 1 "Short" H 3104 5155 50  0000 L CNN
F 2 "" H 3100 5300 50  0001 C CNN
F 3 "~" H 2900 5200 50  0001 C CNN
	1    2900 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2500 3000 2500
Wire Wire Line
	3000 2500 3000 5000
Wire Wire Line
	3000 5400 3000 5500
Connection ~ 3000 5500
Wire Wire Line
	3000 5500 4500 5500
Connection ~ 3000 2500
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5FBBCE32
P 4400 5200
F 0 "Q?" H 4604 5246 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 4604 5155 50  0000 L CNN
F 2 "" H 4600 5300 50  0001 C CNN
F 3 "~" H 4400 5200 50  0001 C CNN
	1    4400 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FBC7E77
P 6000 3300
F 0 "D?" V 6046 3221 50  0000 R CNN
F 1 "D" V 5955 3221 50  0000 R CNN
F 2 "" H 6000 3300 50  0001 C CNN
F 3 "~" H 6000 3300 50  0001 C CNN
	1    6000 3300
	0    -1   -1   0   
$EndComp
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5FBC7E81
P 5900 3650
F 0 "Q?" H 6104 3696 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 6104 3605 50  0000 L CNN
F 2 "" H 6100 3750 50  0001 C CNN
F 3 "~" H 5900 3650 50  0001 C CNN
	1    5900 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5FBC7E8B
P 4400 3650
F 0 "Q?" H 4604 3696 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 4604 3605 50  0000 L CNN
F 2 "" H 4600 3750 50  0001 C CNN
F 3 "~" H 4400 3650 50  0001 C CNN
	1    4400 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 5FBC7E95
P 4500 3300
F 0 "D?" V 4546 3221 50  0000 R CNN
F 1 "D" V 4455 3221 50  0000 R CNN
F 2 "" H 4500 3300 50  0001 C CNN
F 3 "~" H 4500 3300 50  0001 C CNN
	1    4500 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 3000 6000 3000
Wire Wire Line
	6000 3000 6000 3150
Wire Wire Line
	4500 3150 4500 3000
Wire Wire Line
	4500 3000 6000 3000
Connection ~ 6000 3000
Wire Wire Line
	4500 3850 4500 4000
Connection ~ 4500 4000
Wire Wire Line
	6000 3850 6000 4500
Connection ~ 6000 4500
$Comp
L Device:D D?
U 1 1 5FBCCCEE
P 7000 3700
F 0 "D?" V 6954 3779 50  0000 L CNN
F 1 "D" V 7045 3779 50  0000 L CNN
F 2 "" H 7000 3700 50  0001 C CNN
F 3 "~" H 7000 3700 50  0001 C CNN
	1    7000 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FBCD201
P 7000 3300
F 0 "R?" H 7070 3346 50  0000 L CNN
F 1 "R" H 7070 3255 50  0000 L CNN
F 2 "" V 6930 3300 50  0001 C CNN
F 3 "~" H 7000 3300 50  0001 C CNN
	1    7000 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3150 7000 2500
Connection ~ 7000 2500
Wire Wire Line
	7000 2500 3000 2500
Wire Wire Line
	7000 3450 7000 3550
$Comp
L Device:D D?
U 1 1 5FBCDBF3
P 7500 3700
F 0 "D?" V 7454 3779 50  0000 L CNN
F 1 "D" V 7545 3779 50  0000 L CNN
F 2 "" H 7500 3700 50  0001 C CNN
F 3 "~" H 7500 3700 50  0001 C CNN
	1    7500 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FBCDBFD
P 7500 3300
F 0 "R?" H 7570 3346 50  0000 L CNN
F 1 "R" H 7570 3255 50  0000 L CNN
F 2 "" V 7430 3300 50  0001 C CNN
F 3 "~" H 7500 3300 50  0001 C CNN
	1    7500 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3150 7500 2500
Wire Wire Line
	7500 3450 7500 3550
Wire Wire Line
	7500 3850 7500 4500
Wire Wire Line
	7000 3850 7000 4000
Connection ~ 7000 4000
Wire Wire Line
	7000 4000 4500 4000
Connection ~ 7500 2500
Wire Wire Line
	7500 2500 7000 2500
Connection ~ 7500 4500
Wire Wire Line
	7500 2500 8000 2500
Wire Wire Line
	7500 4500 8000 4500
Wire Wire Line
	6000 4500 6000 5000
Wire Wire Line
	4500 4000 4500 5000
Text Notes 4600 2950 0    50   ~ 0
diodes to force currents in correct\ndirection when short is activated
Text Notes 6950 2400 0    50   ~ 0
free-wheeling diodes\nwith extra resistance\nfor faster decay
Wire Notes Line
	6800 2100 6800 4700
Wire Notes Line
	6800 4700 7850 4700
Wire Notes Line
	7850 4700 7850 2100
Wire Notes Line
	7850 2100 6800 2100
$EndSCHEMATC

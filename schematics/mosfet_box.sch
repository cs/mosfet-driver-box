EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4250 3000 1500 1050
U 5FBBC4BD
F0 "Single H Bridge" 50
F1 "singleH.sch" 50
$EndSheet
$Sheet
S 6500 3000 1450 850 
U 5FBBC4DF
F0 "Dual H Bridge" 50
F1 "dualH.sch" 50
$EndSheet
$EndSCHEMATC
